# girlsfrontline-tools

## Useful Links
- https://opencv-python-tutroals.readthedocs.io/en/latest/index.html
- https://github.com/kwhat/libscreenshot/blob/master/src/x11/XGetImage.c
- http://python-xlib.sourceforge.net/doc/html/python-xlib_toc.html

## File Formats
|Extension|Magic|Description|Other Info|
|---------|-----|-----------|----------|
|ab|UnityWeb|Unity Asset Bundle||
|acb.bin|@UTF|JP Voices - CriWare Atom CueSheet Binary?|CRI Atom Craft Ver.2.24.02 - ACB Format/PC Ver.1.28.02 Build - http://blog.criware.com/index.php/2017/06/08/using-the-atom-viewer/|
|cg|N/A|GLES Shader|Note: some directories have a .cg which is hidden|
|obj|v|Vector Keystore?|Contains a bunch of lines with vector like data. See `Terrain.obj example contents` below.|
|Mesh.pickle|\x80\x03ccollections|Model/Bones||
|txt| ()|Scene Subtitles||
|Language_AVG_US.txt|Text|Keymap for Subtitles||
|atlas.txt|Text|UV Mapping||
|skel.bin|\x1C|Binary format that uses the UV maps|`mine.skel.bin` is a single bone model - good for RE|
|model.txt|\x00|Live2D something|Seems to be the same for all backgrounds. Characters are different.|
|moc.bin|\x00|Live2D something|Different with every background / character|
|mtn.txt|\x00|Live2D something|Different everywhere|
|physics.txt|\x00|Live2D something|Different everywhere|

## adb commands
- `screenrecord --output-format=h264 -`

## Terrain.obj example contents
```
v 8.770188331604004 -1.9895981550216675 0.923793613910675
v 8.770187377929688 -1.8633835315704346 1.2433907985687256
vn 2.2134399841888808e-05 1.0 0.0
vn -1.0 -1.2282994248380419e-05 0.0
vt 0.5643852949142456 0.6586675047874451
vt 0.5541560649871826 0.6341231465339661
g Terrain
s 1
usemtl Terrain
f 3/3/3 2/2/2 1/1/1
f 4/4/4 1/1/1 2/2/2
```
