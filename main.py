#!/bin/env python3
import os
import numpy
import cv2
import time
from IPython import embed
from matplotlib import pyplot
from adb.client import Client as AdbClient
from templates import logistics, confirm, mmstoreplus, close

# Lazy globals for assets
ASSETS_RES = "1024x768"
ASSETS_LANG = "en_US"

client = AdbClient(host="127.0.0.1", port=5037)
confirm_img = cv2.imread("assets/" + ASSETS_RES + "/" + ASSETS_LANG + "/confirm.png", 0)
confirm_img_h, confirm_img_w = confirm_img.shape

# Connect to the device and screenshot
devices = client.devices()
device = devices[0] if devices else None # Just pick the first device for now
if (not device):
    print("no device")
    os.exit(1)

# Create template classes
logistics_matcher = logistics.Logistics(ASSETS_RES, ASSETS_LANG)
confirm_matcher = confirm.Confirm(ASSETS_RES, ASSETS_LANG)
close_matcher = close.Close(ASSETS_RES, ASSETS_LANG)
mainmenu_matcher = mmstoreplus.StorePlus(ASSETS_RES, ASSETS_LANG)

# Loop
while True:
    # Take screenshot from ADB
    img_bytes = device.screencap()
    img_color = cv2.imdecode(numpy.frombuffer(img_bytes, numpy.uint8), cv2.IMREAD_COLOR) # BGR colormode
    img = cv2.cvtColor(img_color, cv2.COLOR_BGR2GRAY)

    logistics_match, logistics_match_x, logistics_match_y = logistics_matcher.IsMatch(img)
    if (logistics_match):
        print("logistics_match")
        offset = 6
        device.input_tap(logistics_match_x + offset, logistics_match_y + offset)
        time.sleep(1)
        continue

    confirm_match, confirm_match_x, confirm_match_y = confirm_matcher.IsMatch(img)
    if (confirm_match):
        print("confirm_match")
        offset = 20
        device.input_tap(confirm_match_x + offset, confirm_match_y + offset)
        time.sleep(1)
        continue

    close_match, close_match_x, close_match_y = close_matcher.IsMatch(img)
    if (close_match):
        print("close_match")
        offset = 20
        device.input_tap(close_match_x + offset, close_match_y + offset)
        time.sleep(1)
        continue

    mainmenu_match, mainmenu_match_x, mainmenu_match_y = mainmenu_matcher.IsMatch(img)
    if (not mainmenu_match):
        print("failed mainmenu_match")
        device.input_tap(5, 60)
        time.sleep(1)
        continue

#out = img_color.copy()
#for m in zip(*loc[::-1]):
#    print(m)
#    cv2.rectangle(out, m, (m[0] + confirm_img_w, m[1] + confirm_img_h), (0,0,255), 1)
#pyplot.imshow(cv2.cvtColor(out, cv2.COLOR_BGR2RGB))
#pyplot.show()
#embed()
