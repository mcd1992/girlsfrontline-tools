import cv2
import numpy

class Confirm:
    def __init__(self, res, lang):
        self.img = cv2.imread("assets/" + res + "/" + lang + "/confirm.png", 0)
        self.img_h, self.img_w = self.img.shape
        self.matches = None
        self.locations = None

    def IsMatch(self, search_img, threshold=0.8):
        self.matches = cv2.matchTemplate(search_img, self.img, cv2.TM_CCOEFF_NORMED)
        self.locations = numpy.where(self.matches >= threshold)
        if (len(self.locations[0]) > 0):
            return True, self.locations[1][0], self.locations[0][0]
        return False, None, None
